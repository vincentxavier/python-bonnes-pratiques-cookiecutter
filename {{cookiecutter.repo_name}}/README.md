# {{cookiecutter.project_name}}

## Setup
```sh
# Create the virtual env
python -m venv .venv

# Activate it
source .venv/bin/activate

# Install dependencies
pip install -r requirements-dev.txt

# Setup pre-commit and pre-push hooks
pre-commit install -t pre-commit
pre-commit install -t pre-push
```

## Credits
This package was inspired by [sourcery-ai/python-best-practices-cookiecutter](https://github.com/sourcery-ai/python-best-practices-cookiecutter) project template.
