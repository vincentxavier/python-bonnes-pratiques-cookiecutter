# Python Best Practices Cookiecutter

Modèle [cookiecutter](https://github.com/audreyr/cookiecutter) de bonnes pratiques inspiré de ce  [billet](https://sourcery.ai/blog/python-best-practices/).

## Fonctionnalités
- Test avec [pytest](https://docs.pytest.org/en/latest/)
- Formattage du code avec [black](https://github.com/psf/black)
- Tri des imports [isort](https://github.com/timothycrosley/isort)
- Typage statique avec [mypy](http://mypy-lang.org/)
- Linting avec [flake8](http://flake8.pycqa.org/en/latest/)
- Git hooks pour vérifier tout avec [pre-commit](https://pre-commit.com/)
- Continuous Integration avec [Gitlab-CI](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/)

## Quickstart
```sh
# Installation de cookiecutter s'il n'est pas présent dans la distribution
pip install cookiecutter

# Use cookiecutter to create project from this template
cookiecutter gl:vincent-xavier/python-bonnes-pratiques-cookiecutter

# Enter project directory
cd <repo_name>

# Initialise git repo
git init

# Installation des dépendances
python -m venv .venv
source .venv/bin/activate
pip install -r requirements-dev.txt
pip install -r requirements.txt

# Actication des hooks pre-commit et pre-push
pre-commit install -t pre-commit
pre-commit install -t pre-push
```

De façon optionnelle, on peut aussi installer `GNU Make` et utiliser un fichier `Makefile`.
